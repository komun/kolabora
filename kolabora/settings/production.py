from .base import *

PRODUCTION = True
DEBUG = False
BASE_URL = "https://kolabora.komun.org"
try:
    from .local import *
except ImportError:
    pass
