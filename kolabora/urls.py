from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.views.generic.base import RedirectView

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

import campaigns.views
import campaigns.payments


urlpatterns = [
    url(r'^$', campaigns.views.home),
    url(r'^django-admin/', admin.site.urls),

    
    url(r'^admin/login/', RedirectView.as_view(url='/accounts/login/', permanent=False), name='login'),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/images/faviconKOLABORA32x32.png', permanent=True), name='login'),


    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    url(r'^pages/', include(wagtail_urls)),
    #url(r'^p/', include(wagtail_urls)),
]


urlpatterns += [
    # url(r'^$', campaigns.views.home, name='home'),

    # url(r'^questions/$', campaigns.views.questions, name='questions'),
    # url(r'^updates/$', campaigns.views.updates, name='updates'),
    # url(r'^campaigns/', include('campaigns.foo.urls')),
    # url(r'^captcha/', include('captcha.urls')), #todelete not compatible with this django
    # url(r'^admin/', include(admin.site.urls)),
    # url(r'^tinymce/', include('tinymce.urls')),
    url(r'^mail/', campaigns.views.mail, name='mail'),
    url(r'^komun/', campaigns.views.page, {'pagename': 'komun'}),
    url(r'^faq/', campaigns.views.page, {'pagename': 'faq'}),
    url(r'^terms/', campaigns.views.page, {'pagename': 'terms'}),
    url('^i18n/', include('django.conf.urls.i18n')),
    
]
if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))
    ]
urlpatterns += i18n_patterns(
        url(r'^accounts/', include('registration.backends.default.urls')), #Registration with email confirmation

        url(r'^$', campaigns.views.home),
        url(r'^c/(?P<slug>[\w-]+)/$', campaigns.views.campaign),
        url(r'^p/(?P<slug>[\w-]+)/$', campaigns.views.choose, name='choose'),
        url(r'^p/(?P<slug>[\w-]+)/(?P<method>[\w-]+)/$', campaigns.payments.approve_payment, name='approve_payment'),
        url(r'^p/(?P<slug>[\w-]+)/(?P<method>[\w-]+)/complete$', campaigns.payments.complete_payment, name='approve_payment'),
    )

#
# for x in os.listdir(os.path.join(settings.PROJECT_PATH, 'templates/pages')):
#     urlpatterns += [
#         url(r'^p/' + x[:-5] + '$', campaigns.views.page, {'pagename': x[:-5]})
#     ]

urlpatterns += [
    url(r'', include(wagtail_urls)),
    # url(r'^.*$', campaigns.views.page, {'pagename': '404'}, name='404'),
]

# handler404 = 'campaigns.views.handler404'
# handler500 = 'campaigns.views.handler500'

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
