from django import forms
from django.contrib import admin
from django.db import models
from django.conf import settings
from django.contrib import messages

from django.utils.translation import activate, get_language, ugettext_lazy as _
from django.core.mail import send_mail
from django.template.loader import get_template
from django.utils.html import strip_tags

from campaigns.models import Order, Reward, Update, Question, Value
from campaigns.models.campaign import get_translation_or_original
from campaigns.models.order import ORDER_STATUS_PAYMENT_RECEIVED, ORDER_STATUS_REWARDS_SENT

class AdminCampaignForm(admin.ModelAdmin):
    list_display = ('id', 'status', 'short_slug', 'goal_currency', 'goal_amount', 'total_raised','percentage', 'first_published_at')
    list_filter = ('status',)
    search_fields = ('id', 'title', 'intro', 'body', )


def send_received_payment_mail(modeladmin, request, queryset):
    # payment_info_phrase = 'Envía {} faircoins a la dirección {}'.format(32, 'fffffffff')
    messages.info(request, 'Email(s) sent')
    for o in queryset:
        print('Sending mail to', o.email)
        email_address = o.email
        language = o.lang
        status = o.status
        o.email_sent = True
        o.status = ORDER_STATUS_PAYMENT_RECEIVED
        activate(language)
        campaign = get_translation_or_original(o.campaign, language)
        if not campaign:
            campaign = o.campaign
        proj_name = campaign.title
        if o.donor_wallet not in ['nocryptos', 'nowallet']:
            address_url = 'https://chain.fair.to/address?address={}'.format(o.donor_wallet)
            if o.blockchain_transaction:
                transaction_url = 'https://chain.fair.to/transaction?transaction={}'.format(o.blockchain_transaction)
            o.status = ORDER_STATUS_REWARDS_SENT
        html_message = get_template('payment/emails/received_payment.html').render(locals())
        plain_message = strip_tags(html_message)
        send_mail(
            subject='{1} - {0}'.format('Kolabora',_('Payment received')),
            message=plain_message,
            html_message=html_message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[email_address, settings.KOLABORA_ADMIN_MAIL],
            fail_silently=False)
        o.save()
        # except Exception as error:
        #     mail_sent = False
        #     mail_error_message = str(error)
        #     print('mail_error', mail_error_message)

send_received_payment_mail.short_description = "Enviar mail de pago confirmado"


class AdminOrderForm(admin.ModelAdmin):
    model = Order
    list_display = ('email', 'get_campaign_codename', 'amount_payment_currency', 'payment_currency', 'created_at', 'status', )
    list_filter = ('status', 'email_sent')
    actions = [send_received_payment_mail]

    def get_campaign_codename(self, obj):
        if obj.campaign:
            return obj.campaign.short_slug()
        else:
            return 'UNKNOWN'
    get_campaign_codename.short_description = 'Campaign'
    get_campaign_codename.admin_order_field = 'campaign__slug'


class AdminRewardForm(admin.ModelAdmin):
    model = Reward
    list_display = ('name', 'min_amount')


class AdminUpdateForm(admin.ModelAdmin):
    model = Update
    list_display = ('created_at', 'subject', 'author')


class QuestionForm(forms.ModelForm):
    # captcha = CaptchaField()
    class Meta:
        model = Question
        exclude = ('created_at',)


class AdminQuestionForm(admin.ModelAdmin):
    model = Question
    list_display = ('created_at', 'name', 'orig')

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(AdminQuestionForm, self).formfield_for_foreignkey(
            db_field, request, **kwargs)
        if db_field.related_model == Question:
            field.label_from_instance = self.get_question_id
        return field

    def get_question_id(self, question):
        return '%s %s' % (str(question.created_at), question.name)


class AdminValueForm(admin.ModelAdmin):
    model = Value
    list_display = ('type', 'created_at', 'value')


from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)
from .models import Campaign, CampaignTranslation

#Choose icon from admin/static_src/wagtailadmin/scss/_variables-icons.scss
class CampaignAdmin(ModelAdmin):
    model = Campaign
    menu_icon = 'home'  # change as required
    menu_label = _('Campaigns')
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('title', 'language', 'total_raised', )
    search_fields = ('title', 'goal_amount', 'intro', 'body')
    # choose_parent_view_class = ContentChooseParentView

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        # Only show campaigns managed by the current user
        if not request.user.is_superuser:
            return qs.filter(owner=request.user)
        else: return qs

# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(CampaignAdmin)

class CampaignTranslationAdmin(ModelAdmin):
    model = CampaignTranslation
    menu_label = _('Translations')
    menu_icon = 'site'  # change as required
    menu_order = 201  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('title', 'language', )
    search_fields = ('title', 'language', 'intro', 'body')
    # choose_parent_view_class = ContentChooseParentView

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        # Only show campaigns managed by the current user
        if not request.user.is_superuser:
            return qs.filter(owner=request.user)
        else: return qs

modeladmin_register(CampaignTranslationAdmin)