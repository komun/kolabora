import datetime
import decimal
import time
from django.core.mail import send_mail
from django.conf import settings
from django.shortcuts import render
from django.template.loader import get_template
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.text import format_lazy
from django.http import HttpResponseRedirect

from django.utils.translation import activate as activate_translations, get_language, gettext_lazy as _
from django.shortcuts import render

from campaigns.models import Order, Reward, Campaign
from campaigns.models.campaign import get_campaign_from_slug, get_translation_or_original

from django.conf import settings

from campaigns.utils import send_bot_msg, get_random_decimal
from campaigns.currency import convert_currency, CURRENCIES
from campaigns.models.order import ORDER_STATUS_WAITING
from kolabora.settings import payment_private

PAY_METHODS = {'bank_eur': {'name': format_lazy('{} / Bank of the Commons', _('€ Bank Transfer')), 'currency': 'eur', 'icon':'icon-euro'},
               'cryptos': {'name': format_lazy('{} (BTC, LTC, ETH)', _('Cryptocurrency Payment')), 'icon': 'icon-btc'}
}


#In case your default instance needs to use other payment methods add them in settings
PAY_METHODS.update(payment_private.CUSTOM_PAY_METHODS)
for slug,p in PAY_METHODS.items():
    p['url_slug'] = slug

def approve_payment(request, slug, method):
    lang = get_language()
    # print('lang', lang)
    activate_translations(lang)
    campaign = Campaign.objects.filter(slug=slug).first()
    proj_name = campaign.title
    if campaign.is_completed:
        msg = _('The funding campaign is finished and no longer accepting new contributions.')
        return render(request, 'error.html', locals())

    if request.method == 'POST':
        form = request.POST.copy()
        # print('form', form)
        # if fd['currency_type'] == 'eur':
        #     fd['amount_eur'] = fd['amount']
        #     fd['amount'] = eur_to_dollars(fd['amount'])
        currency_type = form['currency_type']
        amount_payment_currency = form['amount']
        goal_currency = campaign.goal_currency.upper()
        # print(currency_type)
        if 'donor_wallet' not in form:
            donor_wallet = None
        else:
            donor_wallet = form['donor_wallet']
        
        amount_eur = convert_currency(amount=float(form['amount']), c_from=currency_type, c_to='EUR')
        
        print('amount_eur', amount_eur)
        if campaign.selling_crypto != 'none' and form['cryptoaddress'] != 'nocryptos':
            form['amount_crypto_receive'] =  float(convert_currency(amount=amount_eur, c_from='EUR', c_to=campaign.selling_crypto))
        else:
            form['amount_crypto_receive'] = 0

        if goal_currency == currency_type.upper():
            form['goal_currency_amount'] = form['amount']
        else:
            form['goal_currency_amount'] = float(convert_currency(amount=amount_eur, c_from='EUR', c_to=campaign.goal_currency))
        request.session['fd'] = form
        return render(request, 'payment/confirm.html', locals())

    #Not POST, will show the forms
    if method == 'cryptos':
        return render(request, 'payment/cryptos.html', locals())
    elif method == 'mercadopago':
        return render(request, 'payment/mercadopago.html', locals())
    elif method in PAY_METHODS:
        currency = PAY_METHODS[method]['currency']
        currency_symbol = CURRENCIES[currency]['symbol']
        return render(request, 'payment/bank.html', locals())
    else:
        msg = _('Invalid Payment Method.')
        return render(request, 'error.html', locals())


def complete_payment(request, slug, method):
    lang = get_language()
    campaign, translation = get_campaign_from_slug(slug, lang)
    if not translation:
        translation = get_translation_or_original(campaign, lang)
    proj_name = translation.title if translation else campaign.title
    print('lang', lang)
    print('proj_name', proj_name)
    goal_currency = campaign.goal_currency
    now = timezone.now()
    time = now.strftime('%Y-%m-%d %H:%M')
    if request.session['fd']:
        form = request.session['fd']
        payment_currency = form['currency_type']
        goal_currency_amount = form['goal_currency_amount']
        if 'amount_crypto_receive' in form:
            amount_crypto_receive = form['amount_crypto_receive']
        else:
            amount_crypto_receive = 0
        amount_payment_currency = form['amount']
        if campaign.selling_crypto == 'none':
            donor_wallet = None
        else:
            donor_wallet = form['donor_wallet'] if form['cryptoaddress'] == 'yeswallet' else form['cryptoaddress']

        url_next = ''
        if method == 'cryptos':
            crypto_address_pay_to = settings.CRYPTO_RECEIVING_ADDR[payment_currency.upper()]
        elif method == 'mercadopago':
            url_next = payment_private.AMOUNT_CHOICES_URL[amount_payment_currency.upper()]
            payment_currency = 'ARS'
        else:
            fiat_method_pay_to = payment_private.FIAT_RECEIVING_METHOD[payment_currency.upper()]        


        print('Active language', lang)
        date_expiring = now + datetime.timedelta(weeks=1)
        notes = form['notes']
        email = form['email']
        namedonor = form['namedonor']
        notify = 'notify' in form and form['notify'] == True
        if form['chat_usr']:
            notes += ' Chat usr: {}'.format(form['chat_usr'])

        o = Order(
            campaign=campaign,
            date_expiring = date_expiring,
            goal_currency_amount=goal_currency_amount,
            amount_payment_currency=amount_payment_currency.upper(),
            payment_currency=payment_currency.upper(),
            pmethod=method,
            donor_wallet=donor_wallet,
            email=email,
            lang=lang,
            namedonor=namedonor,
            notes=notes,
            status=ORDER_STATUS_WAITING,
            notify=notify,
            cryptoamount_to_send=amount_crypto_receive
        )
        o.save()

        try:
            bot_msg = '*NEW KOLABORA CONTRIBUTION*\nCampaign: {}\nEmail: {}\n{}{} == {}{} \nName: {}\nNotes: {}\nWallet: {}\nFaircoins: {}\nRef: {}'.format(
                            campaign.title, email, 
                            amount_payment_currency, payment_currency, 
                            goal_currency_amount, campaign.goal_currency,
                            namedonor, notes, donor_wallet, amount_crypto_receive, o.pk)
            send_bot_msg(bot_msg)
        except Exception as error:
                bot_error = str(error)
                print('tlg_error', bot_error)

        if campaign.selling_crypto == 'none':
            donor_wallet = None
        else:
            wallet_type = form['cryptoaddress']
            if donor_wallet in ['nocryptos', 'nowallet']:
                donor_wallet = None
        
        #Sending emails
        try:
            html_message = get_template('payment/emails/notify.html').render(locals())
            plain_message = strip_tags(html_message)
            send_mail(
                subject='{1} - {0}'.format(proj_name,_('Thank you for your contribution')),
                message=plain_message,
                html_message=html_message,
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[email],
                fail_silently=False)
            mail_sent = True

            email_campaign = campaign.owner.email
            if email_campaign:
                recipient_list = [email_campaign]
                if settings.PRODUCTION:
                    recipient_list.append(settings.KOLABORA_ADMIN_MAIL)
                html_message = get_template('payment/emails/notify_admins.html').render(locals())
                send_mail(
                    subject='{1} - {0}'.format(proj_name, 'New application for your Kolabora campaign'),
                    html_message=html_message,
                    message=plain_message,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    recipient_list=recipient_list,
                    fail_silently=False)

        except Exception as error:
            mail_sent = False
            mail_error_message = str(error)
            print('mail_error', mail_error_message)

        request.session['fd'] = {}
        if url_next:
            return HttpResponseRedirect(url_next)
        return render(request, 'payment/success.html', locals())
    else:
        msg = _('Your session data could not be found. Please retry your submission again.')
        return render(request, 'error.html', locals())

