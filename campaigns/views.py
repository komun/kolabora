import datetime
import os
import logging
from textwrap import dedent
import json

from django.core.mail import send_mail
from django.shortcuts import render, render_to_response
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib import messages
from django.http import Http404
from django.db.models import Q
from django.http import HttpResponseRedirect

from django.template.loader import get_template
from django.template import RequestContext
import django.urls  as urlresolvers
from django.views.generic import TemplateView
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.views.decorators.csrf import csrf_exempt,csrf_protect #Add this
from django.utils.translation import get_language, activate as activate_translations, ugettext_lazy as _
from django.utils.functional import keep_lazy, keep_lazy_text

from django.urls import reverse_lazy
from django.http import HttpResponse

from campaigns.currency import get_crypto_rate, dollars_to_eur, dollars_to_gbp, eur_to_dollars, gbp_to_dollars
from campaigns.models import Order, Reward, Update, Question, CampaignIndex, Campaign, CampaignTranslation
from campaigns.models.order import ORDER_STATUS_WAITING, ORDER_STATUS_PAYMENT_RECEIVED, ORDER_STATUS_REWARDS_SENT
from campaigns.models.campaign import CURRENCIES, get_campaign_from_slug, CAMPAIGN_STATUS_ACTIVE
from campaigns.forms import QuestionForm
from campaigns.utils import get_page, get_komun_social_urls
from campaigns import payments

from kolabora import urls

PAGES = []
# for x in os.listdir(os.path.join(settings.PROJECT_PATH, 'templates/pages')):
#     PAGES.append((x[:-5], x[:-5].capitalize()))

def handler404(request, template_name="404.html"):
    response = render_to_response("404.html")
    response.status_code = 404
    return response


def handler500(request, *args, **argv):
    response = render_to_response('500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response

def intWithCommas(x):
    if x < 0:
        return '-' + intWithCommas(-x)
    result = ''
    while x >= 1000:
        x, r = divmod(x, 1000)
        result = ",%03d%s" % (r, result)
    return "%d%s" % (x, result)


@csrf_exempt
def mail(request):
    if request.method == 'POST':
        name = request.POST['form_name']
        contact_email = request.POST['form_email']
        subject = request.POST['form_subject']
        phone = request.POST['form_phone']
        message = request.POST['form_message']
        botcheck = request.POST['form_botcheck']

        subject = subject if subject else 'New Message | Contact Form'
        subject = 'Kolabora: {}'.format(subject)

        botcheck = request.POST['form_botcheck']


        reply_msg = 'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.<br />'
        status = False

        if not botcheck:
            msg = "Nombre: {}\nEmail: {}\nUsuari@ Chat: {}\nMensaje: {}".format(name, contact_email, phone, message)
            try:
                # print('sending mail', contact_email, msg )
                send_mail(
                    subject=subject,
                    message=msg,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    recipient_list=[settings.KOLABORA_ADMIN_MAIL],
                    fail_silently=False)
                reply_msg = _('We have <strong>successfully</strong> received your message and will get back to you as soon as possible.')
                status = True
            except Exception as error:
                mail_error_message = str(error)
                reply_msg = mail_error_message

        output = {'message':reply_msg, 'status': status}
        return HttpResponse(json.dumps(output), content_type='application/json')

def questions(request):
    c = get_context()
    c['activepage'] = 'questions'
    c['questions'] = Question.objects.filter(orig=None).order_by('created_at').reverse()
    if request.method == 'POST':
        c['form'] = QuestionForm(request.POST)
        if c['form'].is_valid():
            f = c['form'].save(commit=False)
            if not f.email and f.notify:
                messages.error(request, "Please provide an email address to receive a notification.")
                return render(request, 'comments.html',  RequestContext(request,  c))
            else:
                f.save()
        else:
            messages.error(request, "An error occurred in validation. Please make sure all fields are complete and correct.")
    else:
        c['form'] = QuestionForm()

    return render(request, 'comments.html', c)

def updates(request):
    c = get_context()
    c['activepage'] = 'updates'
    c['updates'] = []
    for u in Update.objects.all().order_by('created_at').reverse():
        c['updates'].append((settings.PROJECT_ADDR+'/updates/#'+str(u.pk), u))
    return render(request, 'updates.html', c)

def choose(request, slug):
    lang = get_language()

    campaign, translation = get_campaign_from_slug(slug, lang) 
    # Campaign.objects.filter(slug=slug).first()
    proj_name = translation.title if translation else campaign.title
    payment_methods = []
    print('payments.PAY_METHODS', payments.PAY_METHODS)
    for m in campaign.payment_methods.split(","):
        if m in payments.PAY_METHODS:
            payment = payments.PAY_METHODS[m]
            print('payment.name', payment["name"])
            payment_methods.append(payment)
    
    if campaign.is_completed:
        msg = _('The funding campaign is not active and does not accept new contributions.')
        return render(request, 'error.html', locals())
    else:
        return render(request, 'payment/choose.html', locals())


def page(request, pagename=''):
    # c = get_context
    c = {}
    c['activepage'] = pagename
    print('pagename', pagename)
    template_name = '{}.html'.format(pagename)
    template_dir = os.path.join(os.path.dirname(__file__), 'templates')
    template_fullpath = os.path.join(template_dir, template_name)
    print('template_fullpath', template_fullpath)
    # template_fullpath = 'index.html'
    return render(request, template_fullpath, c)

@keep_lazy_text
def get_kolabora_phrase(crypto_name):
    phrase = _('Help the campaign and get {}').format(crypto_name)
    return phrase

@keep_lazy_text
def crypto_donate_phrase(crypto_name):
    phrase = _('Donate {} to the campaign').format(crypto_name)
    return phrase

def campaign(request, slug='not_found'):
    # c = get_context
    lang = get_language()
    print('lang', lang)
    activate_translations(lang)
    BASE_URL = settings.BASE_URL
    redirect = False
    # c['activepage'] = pagename
    # print('pagename', pagename)
    # template_name = '{}.html'.format(pagename)
    campaign = Campaign.objects.filter(slug=slug).first()
    if campaign and campaign.language != lang:
        trans_mylang = campaign.translations.filter(language=lang).first()
        if trans_mylang:
            redirect = True
            slug = trans_mylang.slug 
    if not campaign:
        translation = CampaignTranslation.objects.filter(slug=slug).first()
        if translation:
            campaign = translation.original_campaign
            if translation.language != lang:
                if campaign.language == lang:
                    slug = campaign.slug
                    redirect = True
                else:
                    trans_mylang = campaign.translations.filter(language=lang).first()
                    if trans_mylang:
                        slug = trans_mylang.slug
                        redirect = True
            else:
                slug = translation.slug
                campaign = translation
        else: 
            return handler404(request)
    
    if redirect:
        return HttpResponseRedirect('/{}/c/{}/'.format(lang, slug))
    
    # if translation:
    #     campaign = translation
    # campaign_i18n = get_campaign(campaign, lang)
    # slug = campaign_i18n.slug

    page_url = '/c/{}'.format(slug)
    if campaign.selling_crypto in CURRENCIES:
        kolabora_phrase = get_kolabora_phrase(CURRENCIES[campaign.selling_crypto]['name'])
        crypto_donation_phrase = crypto_donate_phrase(CURRENCIES[campaign.selling_crypto]['name'])
    else:
        kolabora_phrase = _('Help the campaign')
        crypto_donation_phrase = ''

    
    # context['page'] = campaign.get_parent().specific
    template_dir = os.path.join(os.path.dirname(__file__), 'templates')
    template_fullpath = os.path.join(template_dir, 'campaign.html')
    komun_social_urls = get_komun_social_urls() 
    # print('template_fullpath', template_fullpath)
    # template_fullpath = 'index.html'
    return render(request, template_fullpath, locals())

from itertools import chain

def home(request, slug='not_found'):
    lang = get_language()
    #Getting campaigns depending on the current language:
    all_campaigns = Campaign.objects.filter(is_completed=False).live()
    
    langs = [lang]
    #exception in case from catalan speakers that all understand Spanish
    if lang in ['ca', 'gl']:   langs.append('es')
    original_campaigns = CampaignTranslation.objects.filter(language__in=langs).values_list('original_campaign', flat=True)
    campaigns_lang = all_campaigns.filter(Q(language__in=langs) | Q(id__in=original_campaigns)).distinct()
    campaigns_lang_ids = campaigns_lang.values_list('id', flat=True)
    not_lang_campaigns = all_campaigns.filter(~Q(id__in=campaigns_lang_ids))
    # print('campaigns_lang', campaigns_lang)
    # print('not_lang_campaigns', not_lang_campaigns)
    campaigns = list(chain(campaigns_lang,  not_lang_campaigns))
    # print('campaigns', campaigns)

    komun_social_urls = get_komun_social_urls()
    telegram_url = komun_social_urls['telegram']
    # total_campaigns_completed = Campaign.objects.filter(is_completed=True).count()
    label = _("Completed Campaigns")
    total_campaigns = all_campaigns.count()
    # context['page'] = campaign.get_parent().specific
    meta_title = _('Kolabora: Crowdfunding by and for activists of the common good')
    page_url = '/'
    meta_description = _('Kolabora helps you finance campaigns by minimizing fees, burocracy and enabling cryptocurrency payments and sell.')
    
    collaborators = Order.objects.filter(status__in=[ORDER_STATUS_PAYMENT_RECEIVED,ORDER_STATUS_WAITING, ORDER_STATUS_REWARDS_SENT]).count()
    faircoins_sent = Order.objects.filter(status=ORDER_STATUS_REWARDS_SENT).aggregate(Sum('cryptoamount_to_send'))['cryptoamount_to_send__sum']
    faircoins_sent = int(faircoins_sent) if faircoins_sent else 0
    euros_obtained = Order.objects.filter(status__in=[ORDER_STATUS_PAYMENT_RECEIVED, ORDER_STATUS_REWARDS_SENT], campaign__goal_currency='eur').aggregate(Sum('goal_currency_amount'))['goal_currency_amount__sum']
    euros_obtained = int(euros_obtained) if euros_obtained else 0
    template_fullpath = os.path.join(os.path.dirname(__file__), 'templates', 'index.html')
    return render(request, template_fullpath, locals())


@receiver(post_save, sender=Update)
def send_notif(sender, instance, **kwargs):
    proj_name = settings.PROJECT_NAME
    proj_addr = settings.PROJECT_ADDR
    for order in Order.objects.all():
        if order.notify:
            send_mail(subject=proj_name+' - New Update',
                message=get_template('update.txt').render({'update': instance, 'proj_name': proj_name, 'proj_addr': proj_addr}),
                from_email=settings.NOTIFY_SENDER,
                recipient_list=[order.email],
                fail_silently=True)

logger = logging.getLogger(__name__)

class Page(TemplateView):
    """Base class for all pages on the site"""
    slug = None
    readmore_buttons = True  # if the text is very long, hide extra paragrapsh behing 'read more' buttons

    def dispatch(self, *args, **kwargs):
        self._set_path_and_page(*args, **kwargs)
        return super(Page, self).dispatch(*args, **kwargs)

    def _set_path_and_page(self, *args, **kwargs):
        self._kwargs = kwargs
        self.path = kwargs.get('path', None)
        print(self.path)
        try:
            self.page = self.get_page(self.path)
            print('page', self.page)
        except Http404:
            self.page = None

    @property
    def template_name(self):
        # if we have a template of the currently requested path, we use that
        # instead of the standard 'page.html'
        if hasattr(self, 'kwargs'):
            path = self.kwargs.get('path', '')
        else:
            path = None
        if path:
            template_name = '%s.html' % path
        if self.slug:
            template_name = '%s.html' % self.slug
        else:
            template_name = 'base.html'
        template_dir = os.path.join(os.path.dirname(__file__), 'templates')
        template_fullpath = os.path.join(template_dir, 'pages', template_name)
        if os.path.exists(template_fullpath):
            return os.path.join('pages', template_name)

        # if we did not find a custom template, we return the standard one
        if getattr(self, '_template_name', None):
            return self._template_name
        return 'page.html'

    def get_page(self, path=None):
        """try to find a BasicPage object with a slug corresponding to path or self.slug"""
        if path:
            slug = path
        else:
            slug = self.slug
        if slug:
            return get_page(slug)

    def get_pages(self):
        pages = models.Page.objects
        return pages.all()

    def get_context_data(self, path=None, **kwargs):
        page = self.page
        pages = self.get_pages()
        pages_in_menu = [p for p in pages if p.slug not in ['home']]
        request = self.request
        lang = request.GET.get('lang', None)
        print('request.LANGUAGE_CODE', request.LANGUAGE_CODE)
        print('HTTP_ACCEPT_LANGUAGE', request.META['HTTP_ACCEPT_LANGUAGE'])
        if lang and request.LANGUAGE_CODE != lang:
            # request.LANGUAGE_CODE = lang
            activate_translations(lang)
        #request.session[settings.LANGUAGE_SESSION_KEY] = 'en'
        #print("request.LANGUAGE_CODE = %s\n" % request.LANGUAGE_CODE)
        context = {
            'path': path,
            'page': page,
            'pages': pages,
            'pages_in_menu': pages_in_menu,
            'settings': settings,
            'home_page': get_page('home'),
            'admin_link': admin_link(page),
            'google_analytics_id': getattr(settings, 'GOOGLE_ANALYTICS_ID', None),
            'home_prefix': '..',
            'request': self.request,
        }
        return context

# from wagtail.contrib.modeladmin.views import ChooseParentView

# class ContentChooseParentView(ChooseParentView):
#     def get(self, request, *args, **kwargs):
#         print('model_name', self.model_name)
#         if self.model_name != 'campaign':
#             return super(ContentChooseParentView, self).get(request, *args, **kwargs)

#         article_index = CampaignIndex.objects.filter(live=True).first()

#         return redirect(self.url_helper.get_action_url(
#             'add', self.app_label, self.model_name, article_index.pk))


class HomePage(Page):
    slug = 'index'

    @property
    def template_name(self):
        template_name = 'index.html'
        template_dir = os.path.join(os.path.dirname(__file__), 'templates')
        template_fullpath = os.path.join(template_dir, template_name)
        #print('template_fullpath', template_fullpath)
        template_fullpath = 'index.html'
        return template_fullpath

    def get_context_data(self, *args, **kwargs):
        print('args', args)
        print('kwargs', kwargs)

        c = super(HomePage, self).get_context_data(*args, **kwargs)
        c.update({
            'activepage': 'index',
            'goal': intWithCommas(settings.GOAL),
            'backers': Order.objects.count(),
            'pct': pct,
            'pct_disp': (int(pct) if total else 0),
            'total': (intWithCommas(int(total)) if total else '0'),
            # 'pages': PAGES,
            'nopay': (True if settings.STOP and (settings.DATE - datetime.datetime.now()).days < 0 else False),
            'days': (settings.DATE - datetime.datetime.now()).days,
            'rewards': sorted(Reward.objects.all(), key=lambda i: i.min_amount),
            'rewards_disclaimer': settings.REWARDS_DISCLAIMER,
            'unum': Update.objects.all().count(),
            'qnum': Question.objects.all().count(),
            'proj_name': settings.PROJECT_NAME,
            'proj_addr': settings.PROJECT_ADDR
            })
        return c

def admin_link(page):
    if page:
        return urlresolvers.reverse('admin:campaigns_page_change', args=[page.pk])
