from django.template import Library

from wagtail.embeds.embeds import get_embed
from wagtail.embeds.exceptions import EmbedException

from campaigns.models.campaign import get_translation_or_original
register = Library()


@register.filter
def get_campaign_i18n(campaign, lang):
    campaign_lang = get_translation_or_original(campaign, lang)
    return campaign_lang

@register.filter
def iframe(video):
    try:
        embed = get_embed(video)
        return embed.html
    except EmbedException:
        # Cannot find embed
        pass
    return None