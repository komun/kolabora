import decimal

from django.template import Library
from campaigns import currency
from campaigns.utils import format_number
register = Library()


@register.filter
def add_diff(value, diff):
	return value + diff

@register.filter
def eur_to_btc(value):
	return format_number(currency.eur_to_crypto(value, 'BTC'))

@register.filter
def eur_to_ltc(value):
	return format_number(currency.eur_to_crypto(value, 'LTC'))

@register.filter
def eur_to_eth(value):
	return format_number(currency.eur_to_crypto(value, 'ETH'))

@register.filter
def eur_to_fair(value):
	return format_number(currency.eur_to_crypto(value, 'FAIR'))

@register.filter
def btc_to_dollars(value):
	return currency.btc_to_dollars(value)

@register.filter
def dollars_to_eur(value):
	return currency.dollars_to_eur(value)

@register.filter
def eur_to_dollars(value):
	return currency.eur_to_dollars(value)

@register.filter
def dollars_to_gbp(value):
	return currency.dollars_to_gbp(value)

@register.filter
def gbp_to_dollars(value):
	return currency.gbp_to_dollars(value)
