# -*- coding: utf-8 -*-

import datetime
from datetime import date

from django import forms
from django.db import models
from django.http import Http404, HttpResponse
from django.utils.dateformat import DateFormat
from django.utils.formats import date_format
from django.core.validators import MinLengthValidator
from django.utils.translation import get_language, ugettext_lazy as _
from django.core.validators import RegexValidator

from django.contrib.auth.models import Group
from registration.signals import user_activated, user_registered
from django.dispatch import receiver
from django.utils.text import format_lazy

from wagtail.admin.forms import WagtailAdminPageForm
from wagtail.admin.edit_handlers import (FieldPanel, FieldRowPanel,
                                         InlinePanel, MultiFieldPanel,
                                         PageChooserPanel, StreamFieldPanel)
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core import blocks
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page, Orderable
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet



from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.tags import ClusterTaggableManager
from taggit.models import Tag as TaggitTag
from taggit.models import TaggedItemBase
from kolabora.settings.base import BASE_URL

from django.conf import settings
from copy import deepcopy
from campaigns.models import  ChoiceArrayField

from .order import ORDER_STATUS_EXPIRED, ORDER_STATUS_PAYMENT_RECEIVED, ORDER_STATUS_REWARDS_SENT, ORDER_STATUS_WAITING
from campaigns.categories import CATEGORIES
from campaigns.currency import CURRENCIES
import collections
from django.db import models
from wagtail.search import index

CAMPAIGN_STATUS_FAILED = 'FAILED'
CAMPAIGN_STATUS_FINISHED = 'FINISHED'
CAMPAIGN_STATUS_ACTIVE = 'ACTIVE'

CAMPAIGN_STATUS = [
    (CAMPAIGN_STATUS_FINISHED, 'FINISHED'),
    (CAMPAIGN_STATUS_FAILED, 'FAILED'),
    (CAMPAIGN_STATUS_ACTIVE, 'ACTIVE'),
]

CAMPAIGN_LANGUAGES = [
    ('es', 'Castellano'),
    ('ca', 'Català'),
    ('sr', 'српски'),
    ('de', 'Deutsch'),
    ('el', 'Ελληνικά'),
    ('en', 'English'),
    ('eo', 'Esperanto'),
    ('fr', 'Français'),
    ('gl', 'Galego'),
    ('nl', 'Hrvatski'),
    ('it', 'Italiano'),
    ('pt', 'Português'),
]

CAMPAIGN_CURRENCIES = []

od = collections.OrderedDict(sorted(CURRENCIES.items()))
for slug, c in od.items():
    CAMPAIGN_CURRENCIES += ((slug, c['name']),)


CAMPAIGN_SELLING_CRYPTOS = [
    ('fair', _('FairCoin')),
    #Translators: None here is referring to not selling any cryptocurrency
    ('none', _("None")),
]

@receiver(user_activated)
@receiver(user_registered)
def user_registered(sender, user, request, **kwargs):
    editors_group = Group.objects.filter(name='Editors').first()
    user.groups.add(editors_group)

# CAT_CHOICES = ()
# od = collections.OrderedDict(sorted(CATEGORIES.items()))
# for cat_slug, cat in od.items():
#     if '.' in cat_slug:
#         cat_name = _("─── {}").format(_(cat['name']))
#     else:
#         cat_name = _("»» {}").format(_(cat['name']))
    
#     new_choice = (cat_slug, cat_name)
#     CAT_CHOICES += (new_choice,)

class CampaignIndex(Page):
    subpage_types = ['Campaign', 'CampaignTranslation']


MAIN_IMG_DIM = {'height': 565, 'width': 750}
HEADER_IMG_DIM = {'height': 100, 'width': 600, 'optimal_height': 270, 'optimal_width': 1280}

class CampaignForm(WagtailAdminPageForm):

    def clean(self):
        cleaned_data = super().clean()

        # im = cleaned_data.get('main_image')
        # if im and (im.height != MAIN_IMG_DIM['height'] or im.width != MAIN_IMG_DIM['width']):
        #     self.add_error('main_image', _('Error! Image size needs to be width={} and height={}px.').format(MAIN_IMG_DIM['width'], MAIN_IMG_DIM['height']))
        
        # im = cleaned_data.get('header_image')
        # if im and (im.height < HEADER_IMG_DIM['height'] or im.width < HEADER_IMG_DIM['width']):
        #     self.add_error('header_image', _('Error! Image is too small!. Minimum {}x{}px.').format(HEADER_IMG_DIM['width'], HEADER_IMG_DIM['height']))

        #TODO: Check if the cleaned_data.language is repeated in any translation or original language of this campaign
        return cleaned_data

class Campaign(Page):
    language = models.CharField(verbose_name=_("Language"), max_length=5, choices=CAMPAIGN_LANGUAGES, default='es')

    intro = models.CharField(verbose_name=_('Short description'), max_length=155, help_text=_("Explain briefly your project."))
    body = RichTextField(verbose_name=_("Long description"), blank=False, features=['image','h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'], help_text=_("Explain your project, with text and images."))
    rewards = RichTextField(verbose_name=_("Rewards"), blank=True, null=True, features=['image', 'h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'], help_text=_("Leave empty if you don't offer rewards."))

    video = models.URLField(_("Campaign video"), blank=True, null=True, help_text=_("Add the link of the video of your campaign. It can be PeerTube, Youtube, Vimeo."))

    date_limit = models.DateField(verbose_name=_("Limit Date Campaign"), null=True, blank=True)
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Header Image"),
        related_name='header_image', help_text=format_lazy(_("Optimal dimensions: {}x{}px."), HEADER_IMG_DIM['optimal_width'], HEADER_IMG_DIM['optimal_height']),
    )
    #You can use https://via.placeholder.com/750x565 for testing
    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=False,
        on_delete=models.SET_NULL,
        related_name='main_image',
        verbose_name=format_lazy("{} ({} x {})", _("Main image"), MAIN_IMG_DIM['width'], MAIN_IMG_DIM['height']),
        help_text=format_lazy(_("Optimal dimensions: {}x{}px."), MAIN_IMG_DIM['width'], MAIN_IMG_DIM['height']),
    )

    goal_amount = models.IntegerField(verbose_name=_("Goal Amount"), null=True, blank=False, default=1,  help_text=_("Amount you need to achieve your goal. If you need another currency please get in contact with the admins."))
    goal_currency = models.CharField(verbose_name=_("Goal Currency"), max_length=4, choices=CAMPAIGN_CURRENCIES, default='eur', help_text=_("Currency that you need to achieve your goal."))

    selling_crypto = models.CharField(verbose_name=_("Cryptocurrency as reward"), max_length=4, choices=CAMPAIGN_SELLING_CRYPTOS, default='fair', help_text=_("Your backers will be able to get this cryptocurrency as a reward for their payments."))

    # categories = ParentalManyToManyField('CampaignCategory', blank=True)
    tags = ClusterTaggableManager(through='CampaignTags', blank=True, verbose_name=_('Tags'), help_text=_('A comma-separated list of tags.'))
    
    #Admin fields
    crypto_donation_address = models.CharField(verbose_name=_("FairCoin address for donations"), max_length=34, null=True, blank=True, validators=[RegexValidator(regex='^[fF][a-zA-Z0-9]{33}$', message=_('Invalid FairCoin address'), code='nomatch')],
                                 help_text=_("FairCoin address to receive donations to exchange later to your backers."))
    status = models.CharField(max_length=10, choices=CAMPAIGN_STATUS, verbose_name=_('Campaign status'),
        default=CAMPAIGN_STATUS_ACTIVE)

    payment_methods = models.CharField(max_length=100,  default="cryptos,bank_eur", blank=True, null=True, verbose_name="Payment methods")#, default=['defect'])
    # category = models.CharField(max_length=30,  choices=CAT_CHOICES, blank=True, null=True, verbose_name="Category")#, default=['defect'])
    is_completed = models.BooleanField(default=False, verbose_name=_("Is finished?"), help_text="Check when the campaign is finished.")
    kolabora_telegram_group_id = models.IntegerField(verbose_name="Working Telegram Group ID",  null=True, blank=True, help_text="The Id of the telegram group used to talk with the admins of the campaign")
    raised = models.IntegerField(null=True, blank=True, help_text ="Keep empty to use the total confirmed total sum.") #only to display the progressbar, changeble via django-admin or automatic
    raised_donations = models.DecimalField(max_digits=16, decimal_places=8, default=0, blank=True, help_text ="Amount that is raised so far in faircoin donations") #only to display the progressbar, changeble via django-admin or automatic


    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('language', widget=forms.Select),
            FieldPanel('intro'),
            FieldPanel('body'),
        ], heading=_("Information")),
        FieldPanel('rewards'),
        ImageChooserPanel('main_image'),
        ImageChooserPanel('header_image'),
        FieldPanel("video"),
        InlinePanel("links", label=_("Social Links"), max_num=5),
        FieldPanel("selling_crypto"),
        FieldPanel("crypto_donation_address"),
        FieldPanel("goal_currency"),
        FieldPanel("goal_amount"),
        InlinePanel("phases", label=_("Phases"), max_num=5),
        # FieldPanel('category', widget=forms.Select),
        FieldPanel('tags'),

    ]

    settings_panels = [
        FieldPanel('slug'),
        FieldPanel('date_limit'),
    ]
    promote_panels = []
    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
        index.SearchField('title'),
        index.FilterField('first_published_at'),
        index.FilterField('language'),
    ]
    subpage_types = []
    parent_page_types = ['CampaignIndex']
    base_form_class = CampaignForm

    class Meta:
        verbose_name = _('Campaign')
        verbose_name_plural = _('Campaigns')

    def get_phases(self):
        return self.phases

    def get_main_image(self):
        return self.main_image

    def get_header_image(self):
        return self.header_image

    def get_links(self):
        return self.links

    def get_video(self):
        return self.video

    def get_rewards(self):
        if self.rewards == '<p></p>':
            return None
        return self.rewards

    def get_tags(self):
        return self.tags

    def goal_currency_symbol(self):
        if self.goal_currency in CURRENCIES:
            return CURRENCIES[self.goal_currency]['symbol']
        else:
            return ''

    def total_raised(self):
        total = self.orders.filter(status__in=[ORDER_STATUS_PAYMENT_RECEIVED, ORDER_STATUS_REWARDS_SENT, ORDER_STATUS_WAITING]).aggregate(models.Sum('goal_currency_amount'))['goal_currency_amount__sum']
        if total is not None:
            return total
        else:
            return 0

    def percentage(self):
        per = int((self.total_raised() / self.goal_amount) * 100)
        return per if per <= 100 else 100

    # def goal_reached(self):
    #     return self.goal - self.total_pledged() <= 0

    def get_admin_display_title(self):
        return '{} (u:{}, l:{})'.format(self.title, self.owner, self.language)

    def get_url_parts(self, *args, **kwargs):
            # site_id, site_root_url, page_url_relative_to_site_root = super().get_url_parts(*args, **kwargs)
            url_relative = '/{}/c/{}'.format(self.language, self.slug)
            return None, BASE_URL, url_relative

    def short_slug(self):
        return self.slug[:10]
    # @property
    # def campaigns_page(self):
    #     return self.get_parent().specific

    # def get_context(self, request, *args, **kwargs):
    #     context = super(Campaign, self).get_context(request, *args, **kwargs)
    #     context['campaigns_page'] = self.campaigns_page
    #     context['post'] = self
    #     return context

LINK_TYPE = [
    ('homepage', _('Homepage')),
    ('facebook', _('Facebook')),
    ('twitter', _('Twitter')),
    ('mastodon', _('Mastodon')),
    ('instagram', _('Instagram')),
    ('videos', _('Video Channel')),
    ('chat', _('Chat Group')),
    ('fediverse', _('Other')),
]


class Links(Orderable):
        page = ParentalKey('Campaign', on_delete=models.CASCADE, related_name='links', help_text="Useful and social network links related to your project.")
        url = models.URLField(blank=False, null=False)
        type_url = models.CharField(verbose_name=_('Type of link'), max_length=10, choices=LINK_TYPE, blank=False, null=False)

        panels = [
            FieldPanel('type_url'),
            FieldPanel('url'),
        ]

class Phases(Orderable):
        page = ParentalKey('Campaign', on_delete=models.CASCADE, related_name='phases')
        title = models.CharField(verbose_name=_('Title'), max_length=155, null=True)
        description = models.CharField(verbose_name=_('Short description'), max_length=155, null=True)
        is_completed = models.BooleanField(default=False, verbose_name=_("Is finished?"), help_text=_("Check when the phase/task is finished."))

        panels = [
            FieldPanel('title'),
            FieldPanel('description'),
            FieldPanel('is_completed'),
        ]

        class Meta:
            verbose_name = _('Phase')
            verbose_name_plural = _('Phases')


class CampaignTags(TaggedItemBase):
    content_object = ParentalKey('Campaign', related_name='campaign_tags')

from wagtail.admin.edit_handlers import PageChooserPanel

class CampaignTranslation(Page):
    #Translators: This field is when creating a Translation page for a campaign, it points to the original campaign that we are translating from.
    original_campaign = models.ForeignKey(
        'Campaign',
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
        related_name='translations',
        verbose_name=_('Translation from campaign')
    )

    language = models.CharField(max_length=5, choices=CAMPAIGN_LANGUAGES, default='en', verbose_name=_("Page language"))
    intro = models.CharField(verbose_name=_("Short description"), max_length=250, null=True, blank=False)
    body = RichTextField(verbose_name=_("Long description"),  blank=False, features=['image','h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'], help_text=_("Explain your project, with text and images."))
    rewards = RichTextField(verbose_name=_("Rewards"), blank=True, null=True, features=['image', 'h4', 'h5', 'h6', 'ol', 'ul', 'hr', 'bold', 'italic', 'link', 'embed'], help_text=_("Leave empty if you don't offer rewards."))
    tags = ClusterTaggableManager(through='CampaignTranslationTags', blank=True, verbose_name=_('Tags'))
    video = models.URLField(_("Campaign video"), blank=True, null=True, help_text=_("Add the link of the video of your campaign. It can be PeerTube, Youtube, Vimeo."))
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Header Image"),
        related_name='translation_header_image', help_text=format_lazy(_("Optimal dimensions: {}x{}px."), HEADER_IMG_DIM['optimal_width'], HEADER_IMG_DIM['optimal_height']),
    )

    main_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True, blank=True,
        on_delete=models.SET_NULL,
        related_name='translation_main_image',
        verbose_name=format_lazy("{} ({} x {})", _("Main image"), MAIN_IMG_DIM['width'], MAIN_IMG_DIM['height']),
        help_text=format_lazy(_("Optimal dimensions: {}x{}px."), MAIN_IMG_DIM['width'], MAIN_IMG_DIM['height']),
    )
    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        PageChooserPanel('original_campaign', 'campaigns.Campaign'),
        MultiFieldPanel([
            FieldPanel('language', widget=forms.Select),
            FieldPanel('intro'),
            FieldPanel('body'),
            FieldPanel('rewards'),
        ], heading=_("Information")),
        
        ImageChooserPanel('main_image'),
        ImageChooserPanel('header_image'),
        FieldPanel("video"),
        InlinePanel("links", label=_("Social Links"), max_num=5),
        InlinePanel("phases", label=_("Phases"), max_num=5),
        FieldPanel('tags'),
    ]

    settings_panels = [
        FieldPanel('slug'),
    ]
    promote_panels = []

    def get_admin_display_title(self):
        return '{} (u:{}, l:{})'.format(self.title, self.owner, self.language)

    def get_url_parts(self, *args, **kwargs):
            # site_id, site_root_url, page_url_relative_to_site_root = super().get_url_parts(*args, **kwargs)
            url_relative = '/{}/c/{}'.format(self.language, self.slug)
            return None, BASE_URL, url_relative


    def get_main_image(self):
        return self.main_image if self.main_image else self.original_campaign.main_image

    def get_header_image(self):
        return self.header_image if self.header_image else self.original_campaign.header_image

    def get_links(self):
        return self.links if self.links else self.original_campaign.links

    def get_phases(self):
        return self.phases if self.phases else self.original_campaign.phases

    def get_rewards(self):
        if self.rewards == '<p></p>':
            return None
        return self.rewards if self.rewards else self.original_campaign.rewards   

    def get_video(self):
        return self.video if self.video else self.original_campaign.video

    def get_tags(self):
        return self.tags if len(self.tags.all()) else self.original_campaign.tags   

    @property
    def selling_crypto(self):
        return self.original_campaign.selling_crypto

    @property
    def crypto_donation_address(self):
        return self.original_campaign.crypto_donation_address

    @property
    def percentage(self):
        return self.original_campaign.percentage

    @property
    def goal_currency_symbol(self):
        return self.original_campaign.goal_currency_symbol

    @property
    def goal_amount(self):
        return self.original_campaign.goal_amount

    @property
    def total_raised(self):
        return self.original_campaign.total_raised
        
    subpage_types = []
    parent_page_types = ['CampaignIndex']
    base_form_class = CampaignForm


    class Meta:
        verbose_name = _("Translation")
        verbose_name_plural = _('Translations')


class CampaignTranslationTags(TaggedItemBase):
    content_object = ParentalKey('CampaignTranslation', related_name='tagged_items')

class TranslationLinks(Orderable):
        page = ParentalKey('CampaignTranslation', on_delete=models.CASCADE, related_name='links', help_text="Useful and social network links related to your project.")
        url = models.URLField(blank=False, null=False)
        type_url = models.CharField(verbose_name=_('Type of link'), max_length=10, choices=LINK_TYPE, blank=False, null=False)

        panels = [
            FieldPanel('type_url'),
            FieldPanel('url'),
        ]

class TranslationPhases(Orderable):
        page = ParentalKey('CampaignTranslation', on_delete=models.CASCADE, related_name='phases')
        title = models.CharField(verbose_name=_('Title'), max_length=155, null=True)
        description = models.CharField(verbose_name=_('Short description'), max_length=155, null=True)
        is_completed = models.BooleanField(default=False, verbose_name=_("Is finished?"), help_text=_("Check when the phase/task is finished."))

        panels = [
            FieldPanel('title'),
            FieldPanel('description'),
            FieldPanel('is_completed'),
        ]

        class Meta:
            verbose_name = _('Phase')
            verbose_name_plural = _('Phases')


def get_campaign_from_slug(slug, language):
    campaign = Campaign.objects.filter(slug=slug).first()
    if campaign:
        return campaign, None
    else:
        translation = CampaignTranslation.objects.filter(slug=slug).first()
        if translation:
            return translation.original_campaign, translation
        else:
            return None, None

def get_translation_or_original(campaign, language):
    if campaign.language == language:
        return campaign
    else:
        qs = campaign.translations.filter(language=language)
        if not qs:
            if language in ['ca', 'gl']: #exception in case from catalan speakers that all understand Spanish
                return get_translation_or_original(campaign, 'es')
            else:
                return campaign
        trans_campaign = qs.first()
        return trans_campaign
