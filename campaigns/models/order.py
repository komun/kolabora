import uuid

from django.conf import settings
from django.db import models

import stripe

from .reward import Reward

ORDER_STATUS_REIMBURSED = 'REIMBU'
ORDER_STATUS_EXPIRED = 'EXPIR'
ORDER_STATUS_WAITING = 'WAIT'
ORDER_STATUS_PAYMENT_RECEIVED = 'PAID'
ORDER_STATUS_REWARDS_SENT = 'SENT'

ORDER_STATUS = [
    (ORDER_STATUS_REIMBURSED, 'REIMBURSED'),
    (ORDER_STATUS_EXPIRED, 'EXPIRED'),
    (ORDER_STATUS_WAITING, 'WAITING'),
    (ORDER_STATUS_PAYMENT_RECEIVED, 'PAYMENT RECEIVED'),
    (ORDER_STATUS_REWARDS_SENT, 'REWARD SENT'),
]

stripe.api_key = "nooooo"

class Order(models.Model):

    created_at = models.DateTimeField(auto_now=True)
    date_closed = models.DateTimeField(default=None, blank=True, null=True)
    date_expiring = models.DateTimeField(default=None, blank=True, null=True)
    amount_payment_currency = models.DecimalField(max_digits=16, decimal_places=8, default=0, verbose_name='Amount Payed')
    payment_currency = models.CharField(max_length=4, default="", verbose_name='Currency used for payment')
    # reward = models.ForeignKey(Reward, related_name='+', verbose_name='Reward Level', blank=True, null=True, on_delete=models.CASCADE)
    goal_currency_amount = models.DecimalField(max_digits=16, decimal_places=2, default=0, verbose_name='Amount in goal currency')
    pmethod = models.CharField(max_length=10, default='', verbose_name='Payment Method')
    lang = models.CharField(verbose_name="Language", max_length=3, default='en')
    email = models.EmailField(verbose_name='Email')
    notify = models.BooleanField(verbose_name="Notify user for updates")
    donor_wallet = models.CharField(max_length=255, default="", verbose_name='Contributor Wallet', null=True, blank=True)
    blockchain_transaction = models.CharField(max_length=255, default="", verbose_name='The blokchain transaction tx to prove that the cryptocurrency was sent to the donor wallet', null=True, blank=True)
    namedonor = models.CharField(max_length=255, default="", verbose_name='Name Contributor', null=True, blank=True)
    notes = models.CharField(max_length=255, default="", verbose_name='Notes', blank=True)
    campaign = models.ForeignKey('Campaign', related_name='orders', blank=True, null=True, on_delete=models.CASCADE)
    recurrent = models.BooleanField(verbose_name="Make this a recurrent donation", default=False)
    status = models.CharField(max_length=10, choices=ORDER_STATUS, verbose_name='status')
    cryptoamount_to_send = models.DecimalField(max_digits=16, decimal_places=8, default=0, verbose_name='Cryptocurrency equivalent amount')
    email_sent = models.BooleanField(verbose_name="Has the received payment email been sent?", default=False)
    # charge_id = models.CharField(max_length=255, verbose_name="Stripe Charge id", null=True)

    def pledge(self, card=None):
        """pledge the amount (i.e. charge cc or pay cryptos or whatever)

        - the card argument (in case of cc payments) is a stripe Token
        """
        charge = stripe.Charge.create(
            amount=self.amount,
            currency="eur",
            source=card,
            description="Pledge for campaigns, thank you!",
        )

        self.charge_id = charge.id
        self.save()

    def reimburse(self):
        #
        # reimburse stripe payment
        #
        if not self.charge_id:
            msg = 'Charge ID not found - cannot reimburse Stripe Payment'
            raise Exception(msg)
        idempotency_key = str(uuid.uuid4())
        refund = stripe.Refund.create(idempotency_key=idempotency_key, charge=self.charge_id)
        if refund.status == 'succeeded':
            # we are happy
            self.status = ORDER_STATUS_REIMBURSED
            self.save()
        else:
            # try again, otherwise give up
            refund = stripe.Refund.create(idempotency_key=idempotency_key, charge=self.charge_id)
            if refund.status == 'succeeded':
                # we are happy
                self.status = ORDER_STATUS_REIMBURSED
                self.save()
            else:
                raise Exception('Could not reimburse: {refund}'.format(refund=refund))

    # def finalize(self):
    #     self.status = ORDER_STATUS_FINAL
    #     self.save()
