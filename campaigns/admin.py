from django.contrib import admin
from campaigns import models
from campaigns.forms import AdminOrderForm, AdminRewardForm, AdminUpdateForm
from campaigns.forms import AdminQuestionForm, AdminValueForm, AdminCampaignForm

admin.site.register(models.Order, AdminOrderForm)
admin.site.register(models.Reward, AdminRewardForm)
admin.site.register(models.Update, AdminUpdateForm)
admin.site.register(models.Question, AdminQuestionForm)
admin.site.register(models.Value, AdminValueForm)
admin.site.register(models.Campaign, AdminCampaignForm)


class PageAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', ]


admin.site.register(models.Page, PageAdmin)

