import datetime
import decimal
import json
from urllib.request import urlopen
import requests

from django.conf import settings
from django.utils import timezone

from campaigns.models import Value
# from utils import format_number

CURRENCIES = {
    'eur': {'name': 'Euro €', 'symbol': '€'},
    'ars': {'name': 'Peso Argentino $', 'symbol': '$'},
    'fair': {'name': 'FairCoin', 'symbol': '⨎'},
    'btc': {'name': 'Bitcoin', 'symbol': 'Btc'},
    'ltc': {'name': 'LiteCoin', 'symbol': 'Ltc'},
    'eth': {'name': 'Ethereum', 'symbol': 'Eth'}
}

FAIRCOIN_PRICE_IN_EUR = 1.2
CRYPTOS = ['BTC', 'LTC', 'ETH', 'FAIR']
def convert_currency(amount=0, c_from='EUR', c_to='EUR'):
    c_from = c_from.upper()
    c_to = c_to.upper()
    if c_from == c_to: return amount
    if c_to in CRYPTOS:
        rate = decimal.Decimal(get_crypto_rate(c_to))
        x = decimal.Decimal(amount) / rate
    elif c_from in CRYPTOS:
        rate = decimal.Decimal(get_crypto_rate(c_from))
        x = decimal.Decimal(amount) * rate
    else:
        rate = decimal.Decimal(get_rate(c_from, c_to))
        x = decimal.Decimal(amount) * rate

    if c_to in CRYPTOS:
        return x.quantize(decimal.Decimal(10) ** -5)
    else:
        return x.quantize(decimal.Decimal(10) ** -2)
    
    


def eur_to_btc(value):
    return eur_to_crypto(value, 'BTC')

def btc_to_eur(value):
    return crypto_to_eur(value, 'BTC')

def eur_to_crypto(value, crypto):
    x = decimal.Decimal(value) / decimal.Decimal(get_crypto_rate(crypto))
    return x.quantize(decimal.Decimal(10) ** -5)

def crypto_to_eur(value, crypto):
    x = decimal.Decimal(get_crypto_rate(crypto)) / decimal.Decimal(value)
    return x.quantize(decimal.Decimal(10) ** -5)

def dollars_to_eur(value):
    x = decimal.Decimal(value) / decimal.Decimal(get_rate('EUR', 'USD'))
    return x.quantize(decimal.Decimal(10) ** -2)

def eur_to_dollars(value):
    x = decimal.Decimal(value) / decimal.Decimal(get_rate('USD', 'EUR'))
    return x.quantize(decimal.Decimal(10) ** -2)

def dollars_to_gbp(value):
    x = decimal.Decimal(value) / decimal.Decimal(get_rate('GBP', 'USD'))
    return x.quantize(decimal.Decimal(10) ** -2)

def gbp_to_dollars(value):
    x = decimal.Decimal(value) / decimal.Decimal(get_rate('USD', 'GBP'))
    return x.quantize(decimal.Decimal(10) ** -2)

def get_api_crypto_compare_value(currency_from, currency_to='EUR'):
    currency_from = currency_from.upper()
    currency_to = currency_to.upper()
    api_url = 'https://min-api.cryptocompare.com/data/generateAvg?fsym={}&tsym={}&e=Kraken,Coinbase,HitBTC'.format(currency_from, currency_to)
    try:
        response = requests.get(url=api_url).json()
        if 'RAW' in response:
            return response['RAW']['OPEN24HOUR']
    except:
        print('Error getting cryptocompare.com API result')
    return None


def get_crypto_rate(currency):
    if currency.upper() == 'FAIR':
        return FAIRCOIN_PRICE_IN_EUR
    current_time = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
    if not Value.objects.filter(type=currency):
        change_value24h = get_api_crypto_compare_value(currency, 'EUR')
        print('change_value24h', change_value24h)
        if change_value24h:
            Value.objects.create(type=currency, value=change_value24h, update=True)
            return change_value24h

    elif ((current_time - Value.objects.filter(type=currency)[0].created_at).days >= 1) and Value.objects.filter(type=currency)[0].update:
        change_value24h = get_api_crypto_compare_value(currency, 'EUR')
        if change_value24h:
            v = Value.objects.filter(type=currency)[0]
            v.value = change_value24h
            v.created_at = current_time
            v.save()

        return change_value24h
    else:
        return Value.objects.filter(type=currency)[0].value
    return None

def get_rate(src='', tgt=''):
    #https://free.currencyconverterapi.com/api/v6/convert?apiKey=sample-api-key&q=USD_PHP&compact=y
    pair = '{0}_{1}'.format(src, tgt)
    url_request = 'https://free.currencyconverterapi.com/api/v6/convert?apiKey={0}&q={1}&compact=y'.format(settings.CURRENCY_API_KEY, pair)
    print('url_request', url_request)
    current_time = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
    if not Value.objects.filter(type=src+'-'+tgt):
        try:
            data = json.loads(urlopen(url_request).read().decode('utf-8'))
            print(data)
            if data[pair]:
                Value.objects.create(type=src+'-'+tgt, value=data[pair]['val'], update=True)
            else:
                raise Exception('Currency API encountered an error: '+str(data))
        except Exception as e:
            raise Exception('An error occurred with Currency API check: '+str(e))
        return Value.objects.filter(type=src+'-'+tgt)[0].value
    elif ((current_time - Value.objects.filter(type=src+'-'+tgt)[0].created_at).days >= 1) and Value.objects.filter(type=src+'-'+tgt)[0].update:
        try:
            data = json.loads(urlopen(url_request).read().decode('utf-8'))
            if data[pair]:
                v = Value.objects.filter(type=src+'-'+tgt)[0]
                v.value = data[pair]['val']
                v.created_at = current_time
                v.save()
        except:
            pass
        return Value.objects.filter(type=src+'-'+tgt)[0].value
    else:
        return Value.objects.filter(type=src+'-'+tgt)[0].value
