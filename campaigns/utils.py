import copy
import urllib
import datetime
import decimal
import random
from telegram import Bot
import threading

from django.http import Http404
from django.utils.translation import get_language

from django.conf import settings

telegram_bot = None

SOCIAL_URLS = {
                'eo': {'twitter': 'https://twitter.com/retbabilejo',
                      'mastodon': 'https://esperanto.masto.host/@komun_eo',
                      'telegram': 'https://t.me/komun_eo'},
                'es': {'twitter': 'https://twitter.com/komunorg',
                        'mastodon': 'https://hostux.social/@komun',
                        'telegram': 'https://t.me/joinchat/C7QpPhJWTuTq3nWysdytkA'},
                'en': {'twitter': 'https://twitter.com/komunorg',
                        'mastodon': 'https://fosstodon.org/@komun_en',
                        'telegram': 'https://t.me/joinchat/C7QpPg1_gkNIZ0EPDOx1MA'}
            }
SOCIAL_URLS['ca'] = SOCIAL_URLS['es']
SOCIAL_URLS['gl'] = SOCIAL_URLS['es']

def send_tlg_message_thread(chat_id, msg, parse_mode):
    global telegram_bot
    if not telegram_bot:
        telegram_bot = Bot(token=settings.TOKEN_BOT)

    # print('Sending following bot msg:')
    # print(msg)
    telegram_bot.send_message(chat_id=chat_id, text=msg, parse_mode=parse_mode)

def send_bot_msg(msg, debug=False):
    if settings.PRODUCTION and not debug:
        CHAT_ID = settings.ADMIN_TLG_GROUP_ID
    else:
        CHAT_ID = settings.DEV_TLG_ID
    bot_thread = threading.Thread(target=send_tlg_message_thread, args=(CHAT_ID,msg,"Markdown",))
    bot_thread.start()

def get_komun_social_urls():
    lang = get_language()
    if lang in SOCIAL_URLS:
        return SOCIAL_URLS[lang]
    else: 
        return SOCIAL_URLS['en']


def get_random_decimal():
    return decimal.Decimal('0.{}'.format(str(random.randint(0, 99)).zfill(2)))

def format_number(num):
    try:
        dec = decimal.Decimal(num)
    except:
        return 'bad'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0'*zeros) + digits
    else:
        val = digits[:delta] + ('0'*tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val

def first_words(s, num_chars=100):
    """return a string of length not more than num_chars with the first words of the given string

    appends "..." if the original string is longer than num_chars
    """
    result = s[:100]
    result = ' '.join(result.split()[:-1])
    if len(s) > 100:
        result += '...'
    return result


def urlencode(d):
    """call urllib.urlencode, but first tries to avoid encoding errors"""
    try:
        return urllib.urlencode(d)
    except UnicodeEncodeError:
        d = copy.copy(d)
        for k in d:
            if type(d[k]) == type(u''):
                d[k] = d[k].encode('utf8')
        return urllib.urlencode(d)


def format_date_for_timeglider(d):
    return '%04d-%02d-%02d 01:00:00' % (d.year, d.month, d.day)


def prettyprint_date(y, m=None, d=None):
    if y and m and d:
        try:
            date = datetime.date(y, m, d)
            return date
        except ValueError:
            return '{d}-{m}-{y}'.format(d=d, m=m, y=y)
        except TypeError:
            return '{d}-{m}-{y}'.format(d=d, m=m, y=y)

    elif y and m:
        return '{m}-{y}'.format(m=m, y=y)
    elif y:
        return '{y}'.format(y=y)


def to_date(y, m=None, d=None):
    if not y:
        return None
    if not m:
        m = 1
    if not d:
        d = 1
    return datetime.date(y, m, d)


def sluggify(s):
    """Turn s into a friendlier URL fragment

    removes underscores, and strips forward slashes from beginning and end

    returns:
        a string
    """
    # XXX make this smarter
    s = s.lower()
    s = s.strip()
    s = s.replace(' ', '-')
    while '--' in s:
        s = s.replace('--', '-')

    if s.startswith('/'):
        s = s[1:]
    if s.endswith('/'):
        s = s[:-1]

    return s


def slugs2breadcrumbs(ls):
    """given a list of slugs, return a list of (title, url) tuples"""
    result = []
    for slug in ls:
        page = get_page(slug=slug)
        result.append(page)
    result = [(page.title, page.get_absolute_url()) for page in result]
    return result


def fix_date(s):
    if s.isdigit() and len(s) == 4:
        s = '%s-1-1' % s
    return s


def get_page(slug, default=None, raise_404=True):
    from campaigns import models
    try:
        page = models.Page.objects.get(slug=slug)
    except models.Page.DoesNotExist:
        if default is None and raise_404:
            msg = 'Could not find Page with slug "%s"' % slug
            raise Http404(msg)
        else:
            return default
    return page
