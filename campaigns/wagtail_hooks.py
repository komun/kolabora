from wagtail.core import hooks
from django.urls import reverse
from django.http import HttpResponse
from django.conf import settings

from django.utils.translation import ugettext_lazy as _
from kolabora.settings.base import BASE_URL, KOLABORA_ADMIN_MAIL

from wagtail.admin import widgets as wagtailadmin_widgets


@hooks.register('construct_explorer_page_queryset')
def show_my_pages_only(parent_page, pages, request):
    if not request.user.is_superuser:
        pages = pages.filter(owner=request.user)

    return pages

@hooks.register('construct_image_chooser_queryset')
def show_my_uploaded_images_only(images, request):
    # Only show uploaded images
    if not request.user.is_superuser:
    # images = images.filter(collection=request.user) #TODO: get the images of the same collection only
        images = images.filter(uploaded_by_user=request.user)

    return images


@hooks.register('register_page_listing_buttons')
def page_listing_buttons(page, page_perms, is_parent=False):
    page_root_id = 2
    url = '/admin/pages/{0}/add_subpage/'.format(page.id)

    if page.id != page_root_id:

        yield wagtailadmin_widgets.Button(
            _('Add Page'),
            reverse('wagtailadmin_pages:add_subpage', args=[page.id]),
            attrs={'title': _("Add a page to '{title}' ").format(title=page.get_admin_display_title())},
            classes={'button', 'button-small', 'bicolor', 'icon', 'white', 'icon-plus'},
            priority=5
        )

from wagtail.admin.action_menu import ActionMenuItem

class SavingDraftMenuItem(ActionMenuItem):
    label = _('Save draft')
    name= 'action-save'

    # def get_url(self, request, context):
    #     return "https://www.youtube.com/watch?v=dNJdJIwCF_Y"

@hooks.register('register_page_action_menu_item')
def register_savingdraft_menu_item():
    return SavingDraftMenuItem(order=10)

@hooks.register('construct_page_action_menu')
def remove_submit_to_moderator_option(menu_items, request, context):
    menu_items[:] = [item for item in menu_items if item.name != 'action-submit']

@hooks.register('construct_page_chooser_queryset')
def show_my_uploaded_images_only(pages, request):
    # Only show uploaded images
    if not request.user.is_superuser:
        pages = pages.filter(owner=request.user)

    return pages

@hooks.register('construct_image_chooser_queryset')
def show_my_uploaded_images_only(images, request):
    # Only show uploaded images
    if not request.user.is_superuser:
        images = images.filter(uploaded_by_user=request.user)

    return images


@hooks.register('construct_main_menu')
def hide_explorer_menu_item_from_nonadmins(request, menu_items):
  if not request.user.is_superuser:
    menu_items[:] = [item for item in menu_items if item.url in [ '/admin/campaigns/campaign/', '/admin/campaigns/campaigntranslation/' ]]

@hooks.register('before_edit_page')
def before_edit_page(request, page):
    # Non admin Users can only edit their own page
    if not (request.user.is_staff or request.user.is_superuser or page.owner == request.user):
        return HttpResponse("Denegat", content_type="text/plain")
    else:
        return page

from django.core.mail import send_mail
@hooks.register('after_create_page')
def notify_after_create_page(request, page):
    # Send an email after creating new campaign
    if settings.PRODUCTION:
        ok = send_mail(
            'Nueva campaña creada',
            'La campaña "{}" ha sido creada. \nEn espera de moderación aquí: {}/admin/'.format(page.title, BASE_URL),
            from_email='Kolabora <kosmo@disroot.org>',
            recipient_list=[KOLABORA_ADMIN_MAIL],
            fail_silently=False,
        )
        print('Sending mail after creating campaign {}. Result = {}'.format(page.id, ok))
    return page


